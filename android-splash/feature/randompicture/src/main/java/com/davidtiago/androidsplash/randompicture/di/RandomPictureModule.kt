package com.davidtiago.androidsplash.randompicture.di

import com.davidtiago.androidsplash.randompicture.BuildConfig
import com.davidtiago.androidsplash.randompicture.data.RandomPictureRepository
import com.davidtiago.androidsplash.randompicture.data.RemoteRandomPictureRepository
import com.davidtiago.androidsplash.randompicture.data.remote.RandonPictureService
import com.davidtiago.androidsplash.randompicture.network.AcceptVersionInterceptor
import com.davidtiago.androidsplash.randompicture.network.PublicAuthorizationInterceptor
import com.davidtiago.androidsplash.randompicture.network.RetrofitClientCreator
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Named

@Module
@InstallIn(ApplicationComponent::class)
abstract class RandomPictureModule {

    @Binds
    abstract fun bindRandomPictureRepository(randomPictureRepository: RemoteRandomPictureRepository):
            RandomPictureRepository

    companion object {
        @Provides
        @Named("apiPublicKey")
        fun provideApiPublicKey(): String = BuildConfig.API_ACCESS_KEY

        @Provides
        @Named("baseUrl")
        fun provideBseUrl(): String = "https://api.unsplash.com/"

        @Provides
        fun provideRetrofitCreator(
            @Named("baseUrl") baseUrl: String,
            acceptVersionInterceptor: AcceptVersionInterceptor,
            publicAuthorizationInterceptor: PublicAuthorizationInterceptor
        ) =
            RetrofitClientCreator(
                baseUrl,
                setOf(acceptVersionInterceptor, publicAuthorizationInterceptor)
            )

        @Provides
        fun provideRandomPictureService(retrofitClientCreator: RetrofitClientCreator): RandonPictureService =
            retrofitClientCreator.create().create(RandonPictureService::class.java)
    }
}
