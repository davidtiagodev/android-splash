package com.davidtiago.androidsplash.randompicture.data

data class RandomPicture(
    val description: String,
    val urls: Urls
)

data class Urls(val regular: String)
