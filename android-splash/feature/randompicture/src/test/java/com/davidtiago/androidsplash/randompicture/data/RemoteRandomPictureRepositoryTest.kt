package com.davidtiago.androidsplash.randompicture.data

import com.davidtiago.androidsplash.randompicture.data.remote.RandonPictureService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class RemoteRandomPictureRepositoryTest {
    @Nested
    @DisplayName("Invokes Service")
    inner class ServiceInvocation {
        @ExperimentalCoroutinesApi
        private val testScope = TestCoroutineScope()

        @ExperimentalCoroutinesApi
        private val fakeRandomPictureService = FakeRandomPictureService(testScope)

        @ExperimentalCoroutinesApi
        private val randomPictureRepository =
            RemoteRandomPictureRepository(fakeRandomPictureService)
        lateinit var randomPicture: RandomPicture

        @ExperimentalCoroutinesApi
        @Test
        fun getRandomPictureShouldReturnResult() {
            testScope.runBlockingTest {
                randomPicture = randomPictureRepository.getRandomPicture()
                kotlin.test.assertEquals(
                    expected = 1,
                    actual = fakeRandomPictureService.invokeCount
                )
                kotlin.test.assertEquals(
                    expected = "Random picture",
                    actual = randomPicture.description
                )
            }
        }

        @ExperimentalCoroutinesApi
        @AfterEach
        fun tearDown() {
            testScope.cleanupTestCoroutines()
        }
    }

    private class FakeRandomPictureService(
        private val scope: CoroutineScope
    ) : RandonPictureService {
        var invokeCount = 0
        override suspend fun get(): RandomPicture {
            scope.launch {
                invokeCount++
                delay(1000) // Simulates network delay
            }.join()
            return RandomPicture(
                description = "Random picture",
                urls = Urls(
                    regular = "regularUrl"
                )
            )
        }
    }
}
