package com.davidtiago.androidsplash.randompicture.network

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class AcceptVersionInterceptorTest {
    private val interceptor = AcceptVersionInterceptor()
    private val server = MockWebServer()

    @BeforeEach
    fun setup() {
        server.enqueue(MockResponse())
        val okHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(interceptor)
            .build()
        okHttpClient.newCall(
            Request.Builder()
                .url(
                    server.url("/")
                )
                .build()
        ).execute()
    }

    @Test
    fun interceptShouldAddVersionHeader() {
        val recordedRequest = server.takeRequest()
        assertEquals(
            expected = "v1",
            actual = recordedRequest.getHeader("Accept-Version")
        )
    }

    @AfterEach
    fun tearDown() = server.shutdown()

}
