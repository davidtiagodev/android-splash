# Continuous Integration Pipeline
A GitLab pipeline was configured for this project, with special focus in static analysis and tests. You can check the pipeline status [here](https://gitlab.com/davidtiagoconceicao/open-checklist-mobile/pipelines). This pipeline executes many steps in a custom docker image container.

## Pipeline setup
The pipeline setup is defined by the [gitlab-ci.yml](open-checklist-android/gitlab-ci.yml) file of the android project.

## Static analysis
Static analysis tools are mandatory for a quick fast and productive development cycle. For this project, two tools are enabled: [detekt](detekt.github.io/detekt/) and [Android Lint](https://developer.android.com/studio/write/lint). Both tools are invoked at master pipeline and every error reported is considered fatal.

## Docker configuration
It is possible to build an Android project using only external images available at Docker Hub. To improve build performance, an specific docker image was created. This saves several steps from the build, improving execution performance and avoiding server overhead. The built image can be found here at Docker Hub with the id `docker.io/davidtiagoconceicao/android-splash`. The Dockerfile for local build can be found in the [android project root](/open-checklist-android/Dockerfile).